package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
@Test
    public void getCounterTest() throws Exception {
    int result = new Increment().getCounter();
    assertEquals("Test Counter Class", 1, result);
}
@Test
    public void decreasecounterTest1() throws Exception{
    int results = new Increment().decreasecounter(0);
    assertEquals("Test Counter Class", 1, results);
}
@Test
    public void decreasecounterTest2() throws Exception{
    int results = new Increment().decreasecounter(1);
    assertEquals("Test Counter Class", 1, results);
}
@Test
    public void decreasecounterTest3() throws Exception{ 
    int results = new Increment().decreasecounter(2);
    assertEquals("Test Counter Class", 1, results);
}
}
